﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckStarScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "asteroid" ||
        collision.gameObject.tag == "star" ||
        collision.gameObject.tag == "heart")
        {
            SpawnControl.isFreeSpaceToSpawn = false;
        }
    }

   
}
