﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject followOn;
    public bool freezeX;
    public int distanceY;
    

	// Update is called once per frame
	void FixedUpdate () {

        if(freezeX)
        this.transform.position = new Vector3(
                transform.position.x,
                followOn.transform.position.y + distanceY,
                transform.position.z
            );

        if (!freezeX)
            this.transform.position = new Vector3(
                    followOn.transform.position.x,
                    followOn.transform.position.y,
                    transform.position.z
                );
    }
}
