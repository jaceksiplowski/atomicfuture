﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

    public bool mayContinue = true;
    public bool onMainMenu = true;

    public Canvas gameOverMenu;
    public Canvas pauseMenu;
    public Canvas scoreCanvas;
    public Canvas mainMenu;

    public static AccountManager accountManager = new AccountManager();

    public VehicleManager vehicleManager;
    public ScoreControl scoreControl;
    public RocketMove rocketMove;

    public GameObject player;
    public AudioControl audioControl;
    public Text GOPointsScore;
    public Text GOCoinScore;
    public Text GOUniqueCoinScore;
    public Text GOHighScore;

    public GameObject GOHighScorePanel;
    public GameObject sceneObjects;

    private void Start()
    {
        accountManager.LoadGameData();
        OpenMainMenu();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape) && !onMainMenu)
        {
            Pause();
        }
    }

    public void StartGame()
    {
        pauseMenu.enabled = false;
        gameOverMenu.enabled = false;
        mainMenu.enabled = false;
        scoreCanvas.enabled = true;
        mayContinue = true;
        onMainMenu = false;

        Time.timeScale = 1;

        audioControl.OnGameAudio();
        vehicleManager.SpawnCurrentVehicle();
        scoreControl.UpdateHeartScores();
    }

    public void OpenMainMenu()
    {
        Time.timeScale = 0;
        ResetGame();

        gameOverMenu.enabled = false;
        pauseMenu.enabled = false;
        gameOverMenu.enabled = false;
        scoreCanvas.enabled = false;
        mainMenu.enabled = true;
        onMainMenu = true;

        vehicleManager.ShowGarage();

        scoreControl.UpdateMainMenuScores();
        audioControl.OnMenuAudio();
    }

    public void Pause()
    {
        if (Time.timeScale != 0 )
        {
            Time.timeScale = 0;
            pauseMenu.enabled = true;
            scoreCanvas.enabled = false;
            audioControl.OnMenuAudio();
        }
        else if(mayContinue)
        {
            Continue();
        }
    }

    public void Continue()
    {
        Time.timeScale = 1;
        pauseMenu.enabled = false;
        scoreCanvas.enabled = true;
        audioControl.OnGameAudio();
    }

    public void GameOver()
    {
        mayContinue = false;
        audioControl.OnMenuAudio();
        Time.timeScale = 0;

        if (scoreCanvas)
            scoreCanvas.enabled = false;

        if (gameOverMenu)
        {
            gameOverMenu.enabled = true;

            GOPointsScore.text = "" + ScoreControl.score;
            GOCoinScore.text = "+" + ScoreControl.targetCount;
            GOUniqueCoinScore.text = "+" + ScoreControl.uniqueCoins;

            if(ScoreControl.IsNewHighScore())
            {
                ScoreControl.SyncHighScore();
                GOHighScorePanel.SetActive(true);
                GOHighScore.text = "" + ScoreControl.highScore;
            }
            else
                GOHighScorePanel.SetActive(false);
        }
            
        accountManager.UpdateDataToStoreAfterLevel();
        accountManager.SaveGameData();
    }

    public void Restart()
    {
        ResetGame();
        StartGame();
    }

    public void ResetGame()
    {
        if(player.transform.childCount > 0 )
            Destroy(player.transform.GetChild(0).gameObject); //destroy vehicle

         DestroyTargets();

        if (sceneObjects.transform.childCount > 0)
        {
            for (int i = 0; i > sceneObjects.transform.childCount - 1; i++)
            {
                Destroy(sceneObjects.transform.GetChild(i).gameObject);
            }
        }
        else
            Debug.Log("no sceneobjects");
        
        scoreControl.ScoreDefault();
        rocketMove.ResetWayPosition();
        rocketMove.ResetPositionToZero();

        ColletiveBonusesSystem.CloseCollection();
    }

    public static void DestroyTargets()
    {
        DestroyObjectsByTag("asteroid");
        DestroyObjectsByTag("planet");
        DestroyObjectsByTag("asteroSet");
        DestroyObjectsByTag("set");
        DestroyObjectsByTag("coinSet");
        DestroyObjectsByTag("target");
        DestroyObjectsByTag("heart");
        DestroyObjectsByTag("claster");
        DestroyObjectsByTag("firework");
        DestroyObjectsByTag("star");
    }

    public static void DestroyObjectsByTag(string tag)
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag(tag);

        if (objects.Length > 0)
        {
            for (int i = objects.Length - 1; i >= 0; i--)
            {
                if(objects[i])
                    Destroy(objects[i]);
            }
        }
    }
}
