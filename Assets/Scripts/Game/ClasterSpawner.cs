﻿using UnityEngine;

public class ClasterSpawner : MonoBehaviour
{
    private GameObject[] clasters;
    public int distance = 20;

    private int position;
    private GameObject objectToSpawn;
    bool isPositionFree = true;

    // Start is called before the first frame update
    void Start()
    {
        clasters = Resources.LoadAll<GameObject>("Clasters");
    }

    // Update is called once per frame
    void Update()
    {
        position = Mathf.RoundToInt(transform.position.y);

        if (position % distance == 0 && isPositionFree)
        {
           
            objectToSpawn = clasters[Random.Range(0, clasters.Length - 1)];

            GameObject spawnedObject = Instantiate(objectToSpawn, new Vector3(0, position, transform.position.z), objectToSpawn.transform.rotation);

            spawnedObject.AddComponent<Rigidbody2D>();
            spawnedObject.AddComponent<BoxCollider2D>();
            spawnedObject.GetComponent<Rigidbody2D>().isKinematic = true;
            spawnedObject.tag = "claster";

            isPositionFree = false;
        }
        else if(position % distance != 0)
        {
            isPositionFree = true;
        }
    }
}
