﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public  class LevelControl : MonoBehaviour
{
    //public GameObject player;
   
    public int level = 1;
    public int bonus;
    public int bonusHeartRate = 3;
    public int totalLevelScore;

    public float levelCompleteStarsCount;
    public float levelCompleteStarsCountDefault = 1;

    public Canvas scoreCanvas;

    private void Start()
    {
        ScoreControl.onAllStars += LevelUp;
        levelCompleteStarsCount = levelCompleteStarsCountDefault;
    }

    public void LevelUp()
    {
        levelCompleteStarsCount = Mathf.Round(levelCompleteStarsCount * 1.5f);

        level++;
    }

    public void ResetLevel()
    {
        level = 1;
        levelCompleteStarsCount = levelCompleteStarsCountDefault;
    }
}
