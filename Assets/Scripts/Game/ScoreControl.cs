﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreControl : MonoBehaviour {

    public delegate void ScoreEventContainer();
    public static event ScoreEventContainer onZeroHearts;
    public static event ScoreEventContainer onAllStars;
    public bool isSpeedUp = false;

    public Text heartCountText;
    public Text starCountText;
    public Text scoreText;
    public Text levelText;
    public Text highScoreText;
    public Text superCoinText;

    public Text coinsMMText;
    public Text uniqueCoinsMMText;

    public GameObject floatingText;

    public Text colItemsCountText;
    public GameObject collectionBonusScore;
    public Image colIcon;

    public int xRateEffectTime = 10;

    public GameObject x2ScoreIcon;
    public GameObject x5ScoreIcon;
    public GameObject x10ScoreIcon;

    public static int heartCount;
    public static int levelUpStarsCount;
    public static int score = 0;
    public static int highScore;
    public static int uniqueCoins = 0;

    public static int targetCount = 0;
    public static int superCoinCount;

    private Color green;
    private Color purple;
    private static int heartDefaultCount;

    private int xRate = 1;

    public LevelControl levelControl;
    public GameObject player;

    // Use this for initialization
    void Start()
    {
        levelControl = this.GetComponent<LevelControl>();

        ColletiveBonusesSystem.onCollectionFinish += MakeReward;

        levelUpStarsCount = (int)levelControl.levelCompleteStarsCount;

        if (starCountText)
            starCountText.text = targetCount + "";

        if (superCoinText)
            superCoinText.text = "" + superCoinCount;

        green = new Color(75, 255, 64);
        purple = new Color(255, 64, 187);

        InvokeRepeating("MakeScoreReewardForMoving", 3f, 1f);
    }

    public void UpdateHeartScores()
    {
        heartCount = GetComponent<VehicleManager>().currentVehicle.GetComponent<StaffEntity>().heartSize;
        heartDefaultCount = heartCount;

        if (heartCountText)
            heartCountText.text = heartCount + "/" + GetComponent<VehicleManager>().currentVehicle.GetComponent<StaffEntity>().heartSize;
    }

    public void CollideReaction()
    {
        --heartCount;

        if (heartCountText)
        {
            heartCountText.text = heartCount + "/" + GetComponent<VehicleManager>().currentVehicle.GetComponent<StaffEntity>().heartSize;
            heartCountText.CrossFadeColor(purple, 3, false, false);
        }

        StartCoroutine(ColorEffect(heartCountText, Color.magenta));

        if (heartCount <= 0)
            this.gameObject.GetComponent<GameControl>().GameOver();

    }

    public void TargetReaction()
    {
        ++targetCount;
        score+=2*xRate;

        if (targetCount >= levelControl.levelCompleteStarsCount)
            levelControl.LevelUp();

        if (starCountText)
        {
            starCountText.text = targetCount + "";
            StartCoroutine(ColorEffect(starCountText, Color.cyan));
        }

        if (scoreText)
            scoreText.text = score + "";
    }

    public void HeartReaction()
    {
        if(heartCount < GetComponent<VehicleManager>().currentVehicle.GetComponent<StaffEntity>().heartSize)
        {
            ++heartCount;

            if (heartCountText)
            {
                heartCountText.text = heartCount + "/" + GetComponent<VehicleManager>().currentVehicle.GetComponent<StaffEntity>().heartSize;
                StartCoroutine(ColorEffect(heartCountText, Color.cyan));
            }
        }
    }

    public void OnXBonus(int _xRate)
    {
        CheckAndResetRate();
        xRate = _xRate;

        if (_xRate == 2)
        {
            x2ScoreIcon.SetActive(true);
            StartCoroutine(xRateDefault(xRateEffectTime, x2ScoreIcon));
        }     
        if (_xRate == 5)
        {
            x5ScoreIcon.SetActive(true);
            StartCoroutine(xRateDefault(xRateEffectTime, x5ScoreIcon));
        }
        if (_xRate == 10)
        {
            x10ScoreIcon.SetActive(true);
            StartCoroutine(xRateDefault(xRateEffectTime, x10ScoreIcon));
        }
    }

    public void OnCollectionBonusReaction()
    {
        ColletiveBonusesSystem.ItemsCountIncrement();
    }

    void CheckAndResetRate()
    {
        if (xRate != 1)
        {
            xRate = 1;
            x10ScoreIcon.SetActive(false);
            x2ScoreIcon.SetActive(false);
            x5ScoreIcon.SetActive(false);
        }
    }

    public IEnumerator xRateDefault(float delay, GameObject iconToHide)
    {
        yield return new WaitForSeconds(delay);
        iconToHide.SetActive(false);
        xRate = 1;
    }

    public void MakeScoreReewardForMoving()
    {
        score += levelControl.level * xRate;

        if (scoreText)
            scoreText.text = score + "";
    }

    private void Update()
    {
        if (starCountText)
            starCountText.text = targetCount + "";

        if (scoreText)
            scoreText.text = score + "";

        if (highScoreText)
            highScoreText.text = highScore + "";

        if (levelText)
            levelText.text = "" + levelControl.level;

        if (superCoinText)
            superCoinText.text = "" + superCoinCount;

        levelUpStarsCount = (int)levelControl.levelCompleteStarsCount;

        if (ColletiveBonusesSystem.IsCollectionOpened)
        {
            collectionBonusScore.SetActive(true);

            colIcon.sprite = ColletiveBonusesSystem.colIcon;

            colItemsCountText.text = ColletiveBonusesSystem.ItemsCount + "/" + ColletiveBonusesSystem.CollectionSize;
        }
        else if(!ColletiveBonusesSystem.IsCollectionOpened)
        {
            collectionBonusScore.SetActive(false);
        }
    }

    public void ScoreDefault()
    {
        if (player.transform.childCount > 0)
            heartCount = player.GetComponentInChildren<StaffEntity>().heartSize;

        targetCount = 0;
        score = 0;

        x10ScoreIcon.SetActive(false);
        x2ScoreIcon.SetActive(false);
        x5ScoreIcon.SetActive(false);
        xRate = 1;

        levelControl.ResetLevel();
        levelControl.levelCompleteStarsCount = levelControl.levelCompleteStarsCountDefault;

        if (scoreText)
            scoreText.text = score + "";
        if (heartCountText)
            heartCountText.text = heartCount + "/" + GetComponent<VehicleManager>().currentVehicle.GetComponent<StaffEntity>().heartSize; ;
        if (starCountText)
            starCountText.text = targetCount + "";
    }

    private IEnumerator ColorEffect(Text textObject, Color color, int waitFor = 1)
    {
        textObject.GetComponent<Text>().color = color;
        yield return new WaitForSeconds(waitFor);
        textObject.GetComponent<Text>().color = Color.white;  
    }

    public static void SyncHighScore()
    {
        if (score > highScore)
        {
            highScore = score;
        }
    }

    public static bool IsNewHighScore()
    {
        if (score > highScore)
            return true;
        else
            return false;
    }

    void MakeReward()
    {
        ++uniqueCoins;
    }
    
    public void UpdateMainMenuScores()
    {
        uniqueCoinsMMText.text = GameControl.accountManager.accountData.uniqueCoins + "";
        coinsMMText.text = GameControl.accountManager.accountData.coins + "";
    }
}
