﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSpawner : MonoBehaviour
{
    public GameObject[] planets;
    public GameObject[] clouds;

    public int distance = 100;
    public int cloudDistance = 100;

    bool isFreePosition = true;
    bool isFreeCloudPosition = true;

    // Start is called before the first frame update
    void Start()
    {
        planets = Resources.LoadAll<GameObject>("Background/Planets");
        clouds = Resources.LoadAll<GameObject>("Background/Clouds");
        Debug.Log("background planets length: "+planets.Length);
        Debug.Log("background cloud length: " + clouds.Length);
    }

    // Update is called once per frame
    void Update()
    {
        int position = Mathf.RoundToInt(transform.position.y);

        if (position % distance == 0 && isFreePosition)
        {
            int planetIndex = Random.Range(0, planets.Length-1 );
            float randomZ = Random.Range(10, 30);
            GameObject spawnedPlanet = Instantiate(planets[planetIndex], new Vector3(Random.Range(-20, 20), transform.position.y, randomZ), transform.rotation);
            spawnedPlanet.layer = 10;

            float scaleRandom = Random.Range(0.5f, 1f);
            spawnedPlanet.transform.localScale = new Vector3(scaleRandom, scaleRandom);
            isFreePosition = false;
        }
        else if(position % distance != 0)
        {
            isFreePosition = true;
        }

        if (position % cloudDistance == 0 && isFreeCloudPosition)
        {
            Debug.Log("bg_cloud");
            int cloudIndex = Random.Range(0, clouds.Length-1);
            float cloudRandomZ = Random.Range(30, 50);
            GameObject spawnedCloud = Instantiate(clouds[cloudIndex], new Vector3(Random.Range(-20, 20), transform.position.y, cloudRandomZ), transform.rotation);
            spawnedCloud.layer = 10;

            float cloudScaleRandom = Random.Range(10f, 50f);
            spawnedCloud.transform.localScale = new Vector3(cloudScaleRandom, cloudScaleRandom);
            isFreeCloudPosition = false;
        }
        else if (position % cloudDistance != 0)
        {
            isFreeCloudPosition = true;
        }
    }
}
