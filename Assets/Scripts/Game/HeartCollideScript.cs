﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartCollideScript : MonoBehaviour {

    public delegate void CollisionContainer();

    public static event CollisionContainer onHeart;

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "heart")
        {
            onHeart();
            Destroy(collider.gameObject);
        }
    }
}
