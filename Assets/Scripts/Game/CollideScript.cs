﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideScript : MonoBehaviour
{
    public delegate void CollisionContainer();

    public static event CollisionContainer onCollision;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "asteroid" || collision.gameObject.tag == "planet" || collision.gameObject.tag == "star")
        {
            onCollision();

            if(ScoreControl.heartCount > 0)
                this.GetComponent<CameraShake>().Shake();
        }
    }

}
