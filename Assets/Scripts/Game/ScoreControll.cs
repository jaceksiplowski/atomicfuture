﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreControll : MonoBehaviour {
    /*
    public delegate void ScoreEventContainer();
    public static event ScoreEventContainer onZeroHearts;
    public static event ScoreEventContainer onAllStars;
    public bool isSpeedUp = false;

    public Text heartCountText;
    public Text starCountText;
    public Text scoreText;
    public Text levelText;
    public Text highScoreText;
    public Text superCoinText;

    public int xRateEffectTime = 10;

    public GameObject x2ScoreIcon;
    public GameObject x5ScoreIcon;
    public GameObject x10ScoreIcon;

    public static int heartCount = 3;
    public static int levelUpStarsCount;
    public static int score = 0;
    public static int highScore;
    public static int uniqueCoins = 0;

    private static int targetCount;
    private static int superCoinCount;

    private Color green;
    private Color purple;
    private static int heartDefaultCount;

    private int xRate = 1;

    // Use this for initialization
    void Start()
    {
        CollideScript.onCollision += CollideReaction;
        TargetCollideScript.onX2 += OnX2;
        TargetCollideScript.onX5 += OnX5;
        TargetCollideScript.onX10 += OnX10;

        TargetCollideScript.onTarget += TargetReaction;
        HeartCollideScript.onHeart += HeartReaction;

        ColletiveBonusesSystem.onCollectionFinish += MakeReward;


        heartDefaultCount = heartCount;

        levelUpStarsCount = (int)LevelControl.levelCompleteStarsCount;

        if(heartCountText)
            heartCountText.text = "" + heartCount;

        if(starCountText)
            starCountText.text = targetCount + " / " + levelUpStarsCount;

        green = new Color(75, 255, 64);
        purple = new Color(255, 64, 187);

     //   highScore = AccountManager.GetHighScore();
    }

    void CollideReaction()
    {
        --heartCount;
        heartCountText.CrossFadeColor(purple, 3, false, false);

        StartCoroutine(ColorEffect(heartCountText, Color.magenta));
    }

    void TargetReaction()
    {
        ++targetCount;
        score+=2*xRate;

        StartCoroutine(ColorEffect(starCountText, Color.cyan));
    }

    void HeartReaction()
    {
        ++heartCount;

        StartCoroutine(ColorEffect(heartCountText, Color.cyan));
    }

    void OnX2()
    {
        CheckAndResetRate();
        xRate = 2;
        x2ScoreIcon.SetActive(true);
        StartCoroutine(xRateDefault(xRateEffectTime, x2ScoreIcon));
    }

    void OnX5()
    {
        CheckAndResetRate();
        xRate = 5;
        x5ScoreIcon.SetActive(true);
        StartCoroutine(xRateDefault(xRateEffectTime, x5ScoreIcon));
    }

    void OnX10()
    {
        CheckAndResetRate();
        xRate = 10;
        x10ScoreIcon.SetActive(true);
        StartCoroutine(xRateDefault(xRateEffectTime, x10ScoreIcon));
    }

    void CheckAndResetRate()
    {
        if (xRate != 1)
        {
            xRate = 1;
            x10ScoreIcon.SetActive(false);
            x2ScoreIcon.SetActive(false);
            x5ScoreIcon.SetActive(false);
        }
    }

    public IEnumerator xRateDefault(float delay, GameObject iconToHide)
    {
        yield return new WaitForSeconds(delay);
        iconToHide.SetActive(false);
        xRate = 1;
    }

    private void Update()
    {
        /*
        if (score < highScore)
        {
            highScore = score;

            Debug.Log("new high score : " + highScore);
        }
        */
    /*
            if (heartCount <= 0)
                onZeroHearts();

            if (targetCount >= levelUpStarsCount)
            {
                onAllStars();
            }

            if (heartCountText)
                heartCountText.text = "" + heartCount;

            if (starCountText)
                starCountText.text = targetCount + "/" + levelUpStarsCount;

            if (scoreText)
                scoreText.text = score + "";

            if (highScoreText)
                highScoreText.text = highScore + "";

            if (levelText)
                levelText.text = "" + levelControl.level;

            if (superCoinText)
                superCoinText.text = "" + superCoinCount;

            levelUpStarsCount = (int)LevelControl.levelCompleteStarsCount;
        }

        public static void ScoreDefault()
        {
            heartCount = heartDefaultCount;
            targetCount = 0;
            score = 0;
            LevelControl.level = 1;
            LevelControl.levelCompleteStarsCount = LevelControl.levelCompleteStarsCountDefault;
        }

        private IEnumerator ColorEffect(Text textObject, Color color, int waitFor = 1)
        {
            textObject.GetComponent<Text>().color = color;
            yield return new WaitForSeconds(waitFor);
            textObject.GetComponent<Text>().color = Color.white;  
        }

        public static void SyncHighScore()
        {
            if (score > highScore)
            {
                highScore = score;

                Debug.Log("new high score : " + highScore);

                // show it on gui
            }
        }

        public void MakeReward()
        {
            superCoinCount++;
        }
        */
}

