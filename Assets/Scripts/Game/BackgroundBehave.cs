﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundBehave : MonoBehaviour {

    public GameObject camera;

    private Vector3 position;

	// Use this for initialization
	void Start () {
        position = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        position.x = camera.transform.position.x;
        position.y = camera.transform.position.y;

        this.transform.position = position;
    }
}
