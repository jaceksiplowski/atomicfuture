﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketMove : MonoBehaviour
{
    public GameObject[] paths;
    public static float speed = 0.07f;
    public static float startSpeed;
    static float speedComplexityStep = 1.1f;

    public GameControl gameControl;

    private int currentPathIndex = 1;

    private static bool reset = false;

    private void Start()
    {
        startSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        speed = startSpeed * Mathf.Pow(speedComplexityStep, gameControl.scoreControl.levelControl.level);

        if (Time.timeScale > 0 && gameControl.mayContinue)
        {
            if ((Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.Keypad4)) && currentPathIndex > 0)
            {
                Turn("left");
            }

            if ((Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow)) || Input.GetKeyUp(KeyCode.Keypad6) && currentPathIndex < paths.Length - 1)
            {
                Turn("right");
            }  

            transform.position = new Vector3(
                    paths[currentPathIndex].transform.position.x,
                    transform.position.y,
                    transform.position.z
                    );        }
    }

    public void Turn(string wayTo)
    {
        if (wayTo == "right")
            currentPathIndex += 1;
        else if (wayTo == "left")
            currentPathIndex -= 1;
        else
            currentPathIndex += 0;

        gameControl.audioControl.PlayWindSfx();
    }

    public  void ResetWayPosition()
    {
        currentPathIndex = 1;
    }

    private void FixedUpdate()
    { 
        if(gameControl.mayContinue)
            transform.position += new Vector3(0, 1, 0) * speed;
    }

    public void ResetPositionToZero()
    {
        transform.position = new Vector3(0, 0, transform.position.z);
    }
}
