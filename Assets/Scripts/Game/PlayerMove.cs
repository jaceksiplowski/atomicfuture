﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public int speed = 1;

    public Vector3 direction;

    // Use this for initialization
    void Start()
    {
        direction = Vector3.up;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = this.transform.position;


            if (Input.GetKeyDown("d") && direction != Vector3.left)
            {
                direction = Vector3.right;
                transform.eulerAngles = new Vector3(0, transform.rotation.y, -90);

            }
            if (Input.GetKeyDown("a") && direction != Vector3.right)
            {
                direction = Vector3.left;
                transform.eulerAngles = new Vector3(0, transform.rotation.y, 90);
            }

            if (Input.GetKeyDown("w") && direction != Vector3.down)
            {
                direction = Vector3.up;
                transform.eulerAngles = new Vector3(0, transform.rotation.y, 0);
            }
            if (Input.GetKeyDown("s") && direction != Vector3.up)
            {
                direction = Vector3.down;
                transform.eulerAngles = new Vector3(0, transform.rotation.y, 180);
            }
       
    }
   
    private void FixedUpdate()
    {
            this.transform.position += direction * Time.deltaTime * speed;
    }
}
