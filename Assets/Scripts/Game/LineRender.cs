﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRender : MonoBehaviour {

    private LineRenderer LineRenderer;
    private int positionCount = 0;
    public GameObject player;

	// Use this for initialization
	void Start () {
        LineRenderer = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        positionCount++;

        LineRenderer.positionCount = positionCount;

        LineRenderer.SetPosition(positionCount-1, player.transform.position);
        
	}
}
