﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetCollideScript : MonoBehaviour
{
    public delegate void CollisionContainer();

    public static event CollisionContainer onTarget;

    public static event CollisionContainer onX2;
    public static event CollisionContainer onX5;
    public static event CollisionContainer onX10;
    public static event CollisionContainer onCollectionBonus;
    public static event CollisionContainer onHeart;
    public static event CollisionContainer onCollision;

    public GameObject yellowFireworkObject;
    public GameObject blueFireworkObject;
    public GameObject greenFireworkObject;
    public GameObject psStar4Object;
    public GameObject psDotObject;
    public GameObject psSparksObject;

    public GameObject psExplosionObject;
    public GameObject psExplosion2Object;
    public GameObject psExplosionHeartObject;
    public GameObject psExplosionBonusObject;

    public GameControl gameControl;

    public GameObject sceneObjects;

    private void Start()
    {
       // gameObject.AddComponent<Collider2D>();
      
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        //Debug.Log("COLLIDE: "+gameObject);
        if (collider.gameObject.tag == "target")
        {
            Destroy(collider.gameObject);

            if(gameControl)
                gameControl.audioControl.PlayCoinSfx();

            InstantiateAndPlayPS(yellowFireworkObject, collider);
          //  InstantiateAndPlayPS(psExplosion2Object, collider);

            if (gameControl)
                gameControl.scoreControl.TargetReaction();
        }

        if (collider.gameObject.tag == "xBonus")
        {
            Destroy(collider.gameObject);

            if (gameControl)
                gameControl.audioControl.PlayBonusSfx();

            InstantiateAndPlayPS(greenFireworkObject, collider);
            InstantiateAndPlayPS(psExplosionBonusObject, collider);

            if (gameControl)
                gameControl.scoreControl.OnXBonus(collider.gameObject.GetComponent<xBonusEntity>().xRate);
        }

        if (collider.gameObject.tag == "collectBonus")
        {
            Destroy(collider.gameObject);

            if (gameControl)
                gameControl.audioControl.PlayBonusSfx();

            InstantiateAndPlayPS(greenFireworkObject, collider);
            InstantiateAndPlayPS(psStar4Object, collider);
            InstantiateAndPlayPS(psExplosionBonusObject, collider);

            if (gameControl)
                gameControl.scoreControl.OnCollectionBonusReaction();
        }

        if (collider.gameObject.tag == "heart")
        {
            Debug.Log(ScoreControl.heartCount + "===" + this.transform.GetComponentInChildren<StaffEntity>().heartSize);
            if (ScoreControl.heartCount < this.transform.GetComponentInChildren<StaffEntity>().heartSize)
            {
                Destroy(collider.gameObject);

                if (gameControl)
                    gameControl.audioControl.PlayHeartSfx();

                InstantiateAndPlayPS(blueFireworkObject, collider);
                InstantiateAndPlayPS(psExplosionHeartObject, collider);

                if (gameControl)
                    gameControl.scoreControl.HeartReaction();
            }
        }

        if (collider.gameObject.tag == "asteroid" || collider.gameObject.tag == "planet" || collider.gameObject.tag == "star")
        {
            if (gameControl)
                gameControl.audioControl.PlayCollisionSfx();

            if (gameControl)
                gameControl.scoreControl.CollideReaction();

            InstantiateAndPlayPS(psSparksObject, collider);
            InstantiateAndPlayPS(psExplosionObject, collider);

            if (ScoreControl.heartCount > 0)
                GetComponentInParent<CameraShake>().Shake();
        }
    }

    public void InstantiateAndPlayPS(GameObject ps, Collider2D collider)
    {
        GameObject instantietedPS = Instantiate(ps, new Vector3(collider.transform.position.x + Random.Range(-0.5f, 0.5f), collider.transform.position.y + Random.Range(-0.5f, 0.5f), collider.transform.position.z - 1), ps.transform.rotation);
        instantietedPS.GetComponent<ParticleSystem>().Play();

        instantietedPS.transform.SetParent(sceneObjects.transform);

        Destroy(instantietedPS, instantietedPS.GetComponent<ParticleSystem>().main.duration);
    }
}
