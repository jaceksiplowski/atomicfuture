﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour {

    private static bool isOnPause = false;

    // Use this for initialization
    void Start () {
        ScoreControl.onZeroHearts += PauseOn;
	}

    public static bool GetPauseStatus()
    {
        return isOnPause;
    }

    void PauseOn()
    {
        isOnPause = true;
    }

    void PauseOff()
    {
        isOnPause = false;
    }
}
