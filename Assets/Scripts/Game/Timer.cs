﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour {

    public Text timerText;

    void Update () {

        int time = Mathf.RoundToInt(Time.timeSinceLevelLoad);
        TimeSpan timeSpan = new TimeSpan(0,0,0,time);

        if (timerText)
            timerText.text = "" + timeSpan.ToString(@"hh\:mm\:ss");
    }
}
