﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColletiveBonusesSystem : MonoBehaviour
{
    public static GameObject bonusObject;
    public delegate void CollectiveBonusHandler();
    public static event CollectiveBonusHandler onCollectionFinish;

    private static bool isCollectionOpened = false;

    private static GameObject[] resources;

    public static Text colCountText;
    public static Sprite colIcon;
    public static GameObject collectionScore;

    public static bool IsCollectionOpened
    {
        get
        {
            return isCollectionOpened;
        }

        private set
        {
            isCollectionOpened = value;
        }
    }

    public static int ItemsCount
    {
        get; set;
    }

    public static int CollectionSize
    {
        get
        {
            return collectionSize;
        }
    }

    private static int collectionSize = 7;
    private static int itemsCount = 0;

    public static void OpenCollection(GameObject obj)
    {
        bonusObject = obj;
        IsCollectionOpened = true;
        ItemsCount = 0;

        colIcon = bonusObject.GetComponent<SpriteRenderer>().sprite;
    }

    public static void CloseCollection()
    {
        bonusObject = null;
        IsCollectionOpened = false;
        ItemsCount = 0;
    }

    private static void FinishCollection()
    {
        CloseCollection();
        onCollectionFinish();
    }

    public static void GenerateCollection()
    {
        int randomIndex = UnityEngine.Random.Range(0, resources.Length - 1);

        OpenCollection(resources[randomIndex]);
    }

    public static void ItemsCountIncrement()
    {
        ++ItemsCount;

        if (ItemsCount >= CollectionSize)
        {
            FinishCollection();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // onCollectionFinish += FinishCollection;
      
        resources = Resources.LoadAll<GameObject>("Interactive/Positive/CollectiveBonuses");
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
