﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ClasterSpawner2 : MonoBehaviour
{
    public GameObject claster;

    public GameObject setPattern;
    public GameObject asteroSet;

    public int distance = 20;

    public GameObject paths;
    public GameObject asteroid;
    public GameObject heart;

    Transform[] slots;

    bool isPositionFree = true;

    GameObject[] barrierSets;
    GameObject[] barrierPlanets;
    GameObject[] barrierStars;

    List<GameObject> allBarriers = new List<GameObject>();

    GameObject[] coinSets;
    GameObject[] xBonuses;

    public GameControl gameControl;

    public GameObject sceneObjects;

    // Start is called before the first frame update
    void Start()
    {
        barrierSets = Resources.LoadAll<GameObject>("Interactive/Negative/Sets");
        barrierPlanets = Resources.LoadAll<GameObject>("Interactive/Negative/Singles/Planets");
        barrierStars = Resources.LoadAll<GameObject>("Interactive/Negative/Singles/Stars");

        coinSets = Resources.LoadAll<GameObject>("Interactive/Positive/CoinSets");
        xBonuses = Resources.LoadAll<GameObject>("Interactive/Positive/XBonuses");
    }

    // Update is called once per frame
    void Update()
    {
        int position = Mathf.RoundToInt(transform.position.y);

        if (position % distance == 0 && isPositionFree && gameControl.mayContinue)
        {
           // Debug.Log("claster spawn");
            // создание "кластеров"
            GameObject spawnedClaster = Instantiate(claster, new Vector3(0, position, transform.position.z), claster.transform.rotation);

            spawnedClaster.AddComponent<Rigidbody2D>();
            spawnedClaster.AddComponent<BoxCollider2D>();
            spawnedClaster.GetComponent<Rigidbody2D>().isKinematic = true;
            spawnedClaster.tag = "claster";
            spawnedClaster.name = "claster spawned";

            spawnedClaster.transform.SetParent(sceneObjects.transform);

            isPositionFree = false;

            //определение количества сетов с препятствиями, которые будут созданы на данном кластере
            int barriersSetLimit = UnityEngine.Random.Range(1, 2);
            int barriersSetCount = 0;

            int positiveLimit = UnityEngine.Random.Range(0, 3 - barriersSetLimit);
            int positiveCount = 0;

            // массив для обозначения занятых либо пустых "путей" на текущем кластере для дальнейшего принятия решения о 
            // создании на них тех или иных сетов с интерактивными обьектами

            bool[] isFreePath = new bool[3];
            isFreePath[0] = true;
            isFreePath[1] = true;
            isFreePath[2] = true;

            // заполнение ранее определенного количества путей препятствиями
            while (barriersSetCount <= barriersSetLimit && positiveCount <= positiveLimit)
            {
                //выбор случайного пути для создания на нём сетов
                int i = UnityEngine.Random.Range(0, 3);

                Transform path = paths.transform.GetChild(i);

                // определение того, создавать "позитивный" или "негативный" сет (препятсвие или монеты/сердце/бонус соответственно)
                int randomSetTypeIndex = UnityEngine.Random.Range(0, 5);

                if (randomSetTypeIndex >= 2)
                {
                    //выбран "негативный" сет

                    if (path != null && isFreePath[i])
                    {
                        if (gameControl.scoreControl.levelControl.level < 5)
                        {
                            GameObject spawnedSet = SpawnSet(asteroSet, spawnedClaster, path);
                            spawnedSet.name = "astero set level <5";

                            spawnedSet.transform.SetParent(sceneObjects.transform);

                        }
                        else if (gameControl.scoreControl.levelControl.level >= 5 && gameControl.scoreControl.levelControl.level < 10)
                        {
                            int randomIndex = UnityEngine.Random.Range(0, 2);

                            if (randomIndex < 1)
                            {
                                GameObject spawnedSet = SpawnSet(asteroSet, spawnedClaster, path);
                                spawnedSet.name = "astero set level 5<10";

                                spawnedSet.transform.SetParent(sceneObjects.transform);
                            }
                            else
                            {
                                GameObject spawnedSet = SpawnPlanetSet(spawnedClaster, path);
                                spawnedSet.name = "planet set level 5>10";

                                spawnedSet.transform.SetParent(sceneObjects.transform);
                            }
                        }
                        else if (gameControl.scoreControl.levelControl.level >= 10)
                        {
                            int randomIndex = UnityEngine.Random.Range(0, 10);

                            if (randomIndex < 1)
                            {
                                GameObject spawnedSet = SpawnSet(asteroSet, spawnedClaster, path);
                                spawnedSet.name = "astero set level >10";

                                spawnedSet.transform.SetParent(sceneObjects.transform);
                            }
                            else if (randomIndex == 1)
                            {
                                GameObject spawnedSet = SpawnPlanetSet(spawnedClaster, path);
                                spawnedSet.name = "planet set level >10";

                                spawnedSet.transform.SetParent(sceneObjects.transform);
                            }
                            else if (randomIndex >= 2 && randomIndex <= 3)
                            {
                                GameObject spawnedSet = SpawnSet(setPattern, spawnedClaster, path);
                                spawnedSet.name = "star system set level>10";

                                SpawnStarSystemSet(spawnedClaster, path);

                                spawnedSet.transform.SetParent(sceneObjects.transform);
                            }
                           
                        }

                        barriersSetCount++;
                        isFreePath[i] = false;
                    }
                }
                else
                {
                    //выбран "позитивный" сет
                    if (path != null && isFreePath[i])
                    {
                        int randomPositiveSetTypeIndex = UnityEngine.Random.Range(0, 12);

                        if (randomPositiveSetTypeIndex == 0)
                        {
                            GameObject spawnedSet = SpawnSet(heart, spawnedClaster, path);
                            spawnedSet.name = "heart positive";

                            spawnedSet.transform.SetParent(sceneObjects.transform);
                        }
                        else if (randomPositiveSetTypeIndex >= 1 && randomPositiveSetTypeIndex < 9)
                        {
                            GameObject positiveSet = coinSets[UnityEngine.Random.Range(0, coinSets.Length - 1)];
                            GameObject spawnedSet = SpawnSet(positiveSet, spawnedClaster, path);

                            spawnedSet.name = "coinset positive";

                            spawnedSet.transform.SetParent(sceneObjects.transform);
                        }
                        else if (randomPositiveSetTypeIndex == 10)
                        {

                            GameObject positiveSet = xBonuses[UnityEngine.Random.Range(0, xBonuses.Length - 1)];

                            GameObject spawnedSet = Instantiate(positiveSet, new Vector3(path.position.x, spawnedClaster.transform.position.y, transform.position.z), positiveSet.transform.rotation);

                            spawnedSet.AddComponent<Rigidbody2D>();
                            spawnedSet.AddComponent<BoxCollider2D>();
                            spawnedSet.GetComponent<Rigidbody2D>().isKinematic = true;

                            spawnedSet.name = "xBonus positive";

                            spawnedSet.transform.SetParent(sceneObjects.transform);
                        }
                        else if (randomPositiveSetTypeIndex >= 11)
                        {

                            if (ColletiveBonusesSystem.IsCollectionOpened)
                            {
                                GameObject spawnedSet = SpawnCollectiveBonus(ColletiveBonusesSystem.bonusObject, spawnedClaster, path);

                                spawnedSet.transform.SetParent(sceneObjects.transform);
                            }
                            else
                            {
                                ColletiveBonusesSystem.GenerateCollection();
                                GameObject spawnedSet = SpawnCollectiveBonus(ColletiveBonusesSystem.bonusObject, spawnedClaster, path);

                                spawnedSet.transform.SetParent(sceneObjects.transform);
                            }
                        }

                        positiveCount++;
                        isFreePath[i] = false;
                    }
                }
            }
        }

        else if (position % distance != 0)
        {
            isPositionFree = true;
        }
    }

    public GameObject SpawnSet(GameObject set, GameObject claster, Transform path)
    {
        GameObject spawnedSet = Instantiate(set, new Vector3(path.position.x, claster.transform.position.y, transform.position.z), set.transform.rotation);
        spawnedSet.GetComponent<Rigidbody2D>().isKinematic = true;

        spawnedSet.transform.SetParent(sceneObjects.transform);

        return spawnedSet;
    }

    public GameObject SpawnCollectiveBonus(GameObject obj, GameObject claster, Transform path)
    {
        GameObject spawnedSet = Instantiate(obj, new Vector3(path.position.x, claster.transform.position.y, transform.position.z), obj.transform.rotation);
        /*
        spawnedSet.AddComponent<Rigidbody2D>();
        spawnedSet.AddComponent<BoxCollider2D>();
        spawnedSet.GetComponent<Rigidbody2D>().isKinematic = true;
        spawnedSet.tag = "collectBonus";
        */
        spawnedSet.transform.SetParent(sceneObjects.transform);

        return spawnedSet;
    }

    public GameObject SpawnPlanetSet(GameObject claster, Transform path)
    {
        GameObject spawnedSet = SpawnSet(setPattern, claster, path);

        foreach (Transform spawnPosTransform in spawnedSet.transform)
        {
            if (spawnPosTransform.name == "spawnPos")
            {
                // генер путя "уникальных препятствий" на определенной позиции в только что сгенерированном сете
                GameObject barrierToSpawn = barrierPlanets[UnityEngine.Random.Range(0, barrierPlanets.Length - 0)];

                float scale = UnityEngine.Random.Range(0.3f, 0.4f);

                GameObject spawnedObject = Instantiate(barrierToSpawn, spawnPosTransform.position, claster.transform.rotation);
                spawnedObject.transform.localScale = new Vector3(scale, scale, 1);

                spawnedObject.AddComponent<Rigidbody2D>();
                spawnedObject.GetComponent<Rigidbody2D>().isKinematic = true;
                spawnedObject.AddComponent<CircleCollider2D>();
                spawnedObject.GetComponent<CircleCollider2D>().radius = 3;

                spawnedObject.tag = "planet";

                spawnedObject.transform.SetParent(sceneObjects.transform);
            }
        }

        return spawnedSet;
    }

    public GameObject SpawnStarSystemSet(GameObject claster, Transform path)
    {
        GameObject spawnedSet = SpawnSet(setPattern, claster, path);

        spawnedSet.transform.SetParent(sceneObjects.transform);

        int starItemIndex = UnityEngine.Random.Range(0, barrierStars.Length-1);

        GameObject spawnedStar = Instantiate(barrierStars[starItemIndex], spawnedSet.transform.GetChild(1).position, claster.transform.rotation);

        spawnedStar.AddComponent<Rigidbody2D>();
        spawnedStar.AddComponent<CircleCollider2D>();
        spawnedStar.GetComponent<CircleCollider2D>().radius = 3;

        spawnedStar.GetComponent<Rigidbody2D>().isKinematic = true;
        spawnedStar.tag = "star";

        spawnedStar.transform.SetParent(sceneObjects.transform);

        GameObject spawnedPlanet1 = Instantiate(barrierPlanets[UnityEngine.Random.Range(0, barrierPlanets.Length - 1)], spawnedSet.transform.GetChild(0).position, claster.transform.rotation);
        GameObject spawnedPlanet2 = Instantiate(barrierPlanets[UnityEngine.Random.Range(0, barrierPlanets.Length - 1)], spawnedSet.transform.GetChild(2).position, claster.transform.rotation);

        spawnedPlanet1.AddComponent<Rigidbody2D>();
        spawnedPlanet1.GetComponent<Rigidbody2D>().isKinematic = true;
        spawnedPlanet1.AddComponent<CircleCollider2D>();
        spawnedPlanet1.GetComponent<CircleCollider2D>().radius = 3;

        spawnedPlanet2.AddComponent<Rigidbody2D>();
        spawnedPlanet2.GetComponent<Rigidbody2D>().isKinematic = true;
        spawnedPlanet2.AddComponent<CircleCollider2D>();
        spawnedPlanet2.GetComponent<CircleCollider2D>().radius = 3;

        spawnedPlanet1.name = "planet1 in star system";
        spawnedPlanet2.name = "planet2 in star system";

        spawnedPlanet1.transform.SetParent(sceneObjects.transform);
        spawnedPlanet2.transform.SetParent(sceneObjects.transform);

        return spawnedSet;
    }
}
