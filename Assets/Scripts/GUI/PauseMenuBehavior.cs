﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuBehavior : MonoBehaviour {

    public Canvas pauseMenu;
    public Canvas scoreCanvas;

	public void Continue()
    {
        Time.timeScale = 1;
        pauseMenu.enabled = false;
        scoreCanvas.enabled = true;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("mainmenu");
    }
}
