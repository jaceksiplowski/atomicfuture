﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MuteButton : MonoBehaviour {

    public Sprite enabledIcon;
    public Sprite disabledIcon;
    public GameObject muteButton;
    bool state = true;

    public void Mute()
    {
        if (state == true)
        {
            muteButton.GetComponent<Image>().sprite = disabledIcon;
            AudioListener.volume = 0;
            
            state = false;
        }
        else if (state == false)
        {
            muteButton.GetComponent<Image>().sprite = enabledIcon;
            AudioListener.volume = 1;
            state = true;
        }
    }
}
