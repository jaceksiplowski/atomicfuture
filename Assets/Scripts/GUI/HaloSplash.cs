﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HaloSplash : MonoBehaviour
{
    public GameObject coinHalo;
    public GameObject heartHalo;

    // Update is called once per frame
    void Start()
    {
        coinHalo.SetActive(false);

        TargetCollideScript.onTarget += CoinSplash;
        HeartCollideScript.onHeart += HeartSplash;
    }

    public void HeartSplash()
    {
        Splash(heartHalo, 0.3f, 0.55f, 0.1f);
    }

    public void CoinSplash()
    {
        Splash(coinHalo, 0.25f, 0.4f, 0.1f);
    }


    public void Splash(GameObject halo, float sizeMin, float sizeMax, float delay)
    {
        float size = Random.Range(sizeMin, sizeMax);
        halo.transform.Rotate(new Vector3(0, 0, 1), Random.Range(120, 360));
        halo.transform.localScale = new Vector3(size, size);

        halo.SetActive(true);

        StartCoroutine(SplashDown(halo, delay));

       
    }

    public IEnumerator SplashDown(GameObject halo, float delay)
    {
        yield return new WaitForSeconds(delay);
        halo.SetActive(false);
    }

   
}