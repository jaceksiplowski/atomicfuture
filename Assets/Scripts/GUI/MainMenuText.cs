﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuText : MonoBehaviour {

    //When the mouse hovers over the GameObject, it turns to this color (red)
    Color m_MouseOverColor = Color.green;

    //This stores the GameObject’s original color
    Color m_OriginalColor;

    public GameObject TextObject;

    void Start()
    {
        m_OriginalColor = TextObject.GetComponent<Text>().color;
    }

    void OnMouseOver()
    {
        GetComponentInChildren<Text>().color = m_MouseOverColor;
    }

    void OnMouseExit()
    {
        GetComponentInChildren<Text>().color = m_OriginalColor;
    }
}
