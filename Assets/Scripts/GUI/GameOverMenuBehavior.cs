﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenuBehavior : MonoBehaviour {

    public Canvas scoreCanvas;
    public Canvas gameOverMenu;
    public Canvas pauseMenu;
    public Canvas mainMenu;
    public GameObject player;
    public GameObject middleWay;

    public GameControl gameControl;

    public void MainMenu()
    {
        // SceneManager.LoadScene("mainmenu");

        player.transform.position = new Vector3(
           middleWay.transform.position.x,
           0,
           player.transform.position.z
           );

        GameControl.DestroyTargets();
        gameControl.scoreControl.ScoreDefault();
        //RocketMove.ResetWayPosition();

        /*
        gameOverMenu.enabled = false;
        pauseMenu.enabled = false;
        scoreCanvas.enabled = false;
        mainMenu.enabled = true;
        */


        ColletiveBonusesSystem.CloseCollection();

        gameControl.OpenMainMenu();
    }

    public void Restart()
    {

        player.transform.position = new Vector3(
            middleWay.transform.position.x,
            0,
            player.transform.position.z
            );

        gameOverMenu.enabled = false;
        pauseMenu.enabled = false;

        scoreCanvas.enabled = true;

        GameControl.DestroyTargets();
        gameControl.scoreControl.ScoreDefault();
       // rocketMove.ResetWayPosition();

       // GameControl.mayContinue = true;

        ColletiveBonusesSystem.CloseCollection();

        Time.timeScale = 1;
        
    }

}
