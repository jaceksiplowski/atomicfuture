﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompleteMenu : MonoBehaviour
{
    public Canvas scoreCanvas;
    public Canvas LevelCompleteCanvas;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Next()
    {
        LevelCompleteCanvas.enabled = false;
        scoreCanvas.enabled = true;

        //RocketMove.ResetWayPosition();

       // GameControl.mayContinue = true;
        Time.timeScale = 1;
    }
}
