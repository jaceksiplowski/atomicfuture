﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    Vector3 cameraPosition;
    public GameObject cameraObject;
    public float shakeMagnitude=0.05f;
    public float shakeTime=0.5f;


    // Start is called before the first frame update
    void Start()
    {
        cameraPosition = cameraObject.transform.position;
    }

    public void Shake()
    {
        cameraPosition = cameraObject.transform.position;
        InvokeRepeating("Shaking", 0f, 0.005f);
        Invoke("StopShaking", shakeTime);
    }


    void Shaking()
    {
        float offsetX = Random.value * shakeMagnitude * 2 - shakeMagnitude;
        float offsetY = Random.value * shakeMagnitude * 2 - shakeMagnitude;

        Vector3 cameraIntermediatePosition = cameraObject.transform.position;
        cameraIntermediatePosition.x += offsetX;
        cameraIntermediatePosition.y += offsetY;

        cameraObject.transform.position = cameraIntermediatePosition;
    }

    void StopShaking()
    {
        CancelInvoke("Shaking");
        cameraObject.transform.position = new Vector3(cameraPosition.x, cameraObject.transform.position.y, cameraPosition.z);
    }

}
