﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]

public class AccountData : MonoBehaviour
{
    public int highScore;
    public int coins;
    public int uniqueCoins;
    public string currentVehicleKey;
    public string ownedVehicles;
    
}
