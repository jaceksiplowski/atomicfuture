﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class VehicleManager : MonoBehaviour
{
    public GameObject[] vehicleContainer;
    public GameObject currentVehicle;
    public GameObject defaultVehicle;

    public GameObject playerObject;

    delegate bool FindVehicle(GameObject gameObject);

    // GUI elements
    public Image vehicleImageInSlot;
    public Text coinPriceText;
    public Text uniqueCoinPriceText;
    public Text vehicleNameText;
    public Text notEnoughMoneyText;

    public Button nextButton;
    public Button previousButton;

    public Canvas shopCanvas;

    public GameObject buyButton;
    public GameObject selectButton;
    public GameObject markImage;

    int vehicleIndex = 0;
    int currentVehicleIndex = 0;

    private GameControl gameControl;

    // Start is called before the first frame update
    void Start()
    {
        gameControl = GetComponent<GameControl>();
        currentVehicle = Array.Find(vehicleContainer, item => item.GetComponent<StaffEntity>().key == GameControl.accountManager.accountData.currentVehicleKey);

        int currentVehicleSearchedIndex = Array.FindIndex(vehicleContainer, item => item.GetComponent<StaffEntity>().key == GameControl.accountManager.accountData.currentVehicleKey);

        if (currentVehicleSearchedIndex != -1)
            currentVehicleIndex = currentVehicleSearchedIndex;
        else
            currentVehicleIndex = 0;

        vehicleIndex = currentVehicleIndex;
        ShowGarage();

        if (currentVehicle == null)
            currentVehicle = vehicleContainer[0];
    }

    public void SelectVehicle()
    {
       if(GameControl.accountManager.accountData.ownedVehicles.Contains(vehicleContainer[vehicleIndex].GetComponent<StaffEntity>().key))
        {
            currentVehicle = vehicleContainer[vehicleIndex];
            currentVehicleIndex = vehicleIndex;

            gameControl.scoreControl.UpdateHeartScores();

            GameControl.accountManager.accountData.currentVehicleKey = currentVehicle.GetComponent<StaffEntity>().key;
            GameControl.accountManager.SaveGameData();

            selectButton.SetActive(false);
            markImage.SetActive(true);
        }
        else
            Debug.Log("vehicle NOT selected");
    }

    public void SpawnCurrentVehicle()
    {
        GameObject spawnedVehicle = Instantiate(currentVehicle, playerObject.transform);
        spawnedVehicle.transform.parent = playerObject.transform;
        spawnedVehicle.transform.SetAsFirstSibling();  
    }

    public void BuyVehicle()
    {
        StaffEntity staff = vehicleContainer[vehicleIndex].GetComponent<StaffEntity>();

        if (GameControl.accountManager.accountData.coins >= staff.coinPrice && GameControl.accountManager.accountData.uniqueCoins >= staff.uniqueCoinPrice)
        {
            vehicleContainer[vehicleIndex].GetComponent<StaffEntity>().isPurchased = true;

            GameControl.accountManager.accountData.coins -= staff.coinPrice;
            GameControl.accountManager.accountData.uniqueCoins -= staff.uniqueCoinPrice;

            GameControl.accountManager.accountData.ownedVehicles += vehicleContainer[vehicleIndex].GetComponent<StaffEntity>().key;

            ShowVehicleInGarageMenu(vehicleIndex);
            gameControl.scoreControl.UpdateMainMenuScores();

            GameControl.accountManager.SaveGameData();
        }
        else
            // Debug.Log("not enough money - your coins:" + GameControl.accountManager.accountData.coins + " ucoins:" + GameControl.accountManager.accountData.uniqueCoins + "- price coins:"+ staff.coinPrice + " ucoins:" + staff.uniqueCoinPrice);

            notEnoughMoneyText.enabled = true;
        }

    public void ShowVehicleInGarageMenu(int index)
    {
        if (vehicleContainer.Length > 0)
        {
            if (index <= vehicleContainer.Length - 1 && index >= 0)
            {
                vehicleImageInSlot.sprite = vehicleContainer[index].GetComponent<SpriteRenderer>().sprite;
                vehicleNameText.text = "" + vehicleContainer[index].GetComponent<StaffEntity>().staffName;

                if (index == 0 || GameControl.accountManager.accountData.ownedVehicles.Contains(vehicleContainer[index].GetComponent<StaffEntity>().key))
                {
                    if(index == currentVehicleIndex)
                    {
                        buyButton.SetActive(false);
                        selectButton.SetActive(false);
                        markImage.SetActive(true);
                    }
                    else
                    {
                        buyButton.SetActive(false);
                        selectButton.SetActive(true);
                        markImage.SetActive(false);
                    }
                }
                else
                {
                    buyButton.SetActive(true);
                    selectButton.SetActive(false);
                    markImage.SetActive(false);
                    notEnoughMoneyText.enabled = false;

                    coinPriceText.text = "" + vehicleContainer[index].GetComponent<StaffEntity>().coinPrice;
                    uniqueCoinPriceText.text = "" + vehicleContainer[index].GetComponent<StaffEntity>().uniqueCoinPrice;

                }
            }
            else
                Debug.Log("veh index [" + index + "] is too for size [" + (vehicleContainer.Length - 1) + "]");

            if (index > 0)
                previousButton.gameObject.SetActive(true);
            else
                previousButton.gameObject.SetActive(false);

            if (index < vehicleContainer.Length - 1)
                nextButton.gameObject.SetActive(true);
            else
                nextButton.gameObject.SetActive(false);
        }
        else
        {
            Debug.Log("vehicle container is empty");
        }
    }

    public void ShowGarage()
    {
        if(vehicleContainer.Length == 0)
            vehicleContainer = Resources.LoadAll<GameObject>("Vehicles");

        // ShowVehicleInGarageMenu(currentVehicleIndex);
        ShowVehicleInGarageMenu(vehicleIndex);
    }

    public void ShowNext()
    {
        if(vehicleIndex < vehicleContainer.Length - 1)
        { 
            ++vehicleIndex;
            ShowVehicleInGarageMenu(vehicleIndex);
        }
    }

    public void ShowPrevious()
    {
        if (vehicleIndex > 0)
        {
            --vehicleIndex;
            ShowVehicleInGarageMenu(vehicleIndex);
       
        }
    }
}
