﻿using UnityEngine;


public class AccountManager : MonoBehaviour
{
    public AccountData accountData = new AccountData();
    public VehicleManager vehicleManager;

    public void UpdateDataToStoreAfterLevel()
    {
        accountData.coins += ScoreControl.targetCount;
        accountData.uniqueCoins += ScoreControl.uniqueCoins;

        accountData.highScore = ScoreControl.highScore;
    }

    public void SaveGameData()
    {
        PlayerPrefs.SetInt("coins", accountData.coins);
        PlayerPrefs.SetInt("uniqueCoins", accountData.uniqueCoins);

        PlayerPrefs.SetInt("highScore", accountData.highScore);
        PlayerPrefs.SetString("vehicleKey", accountData.currentVehicleKey);

        if (accountData.ownedVehicles == "")
            accountData.ownedVehicles += "nuclear/";

        PlayerPrefs.SetString("ownedVehicles", accountData.ownedVehicles);

        PlayerPrefs.Save();
    }

    public void ResetSavedGameData()
    {
        PlayerPrefs.SetInt("coins", 0);
        PlayerPrefs.SetInt("uniqueCoins", 0);
        PlayerPrefs.SetInt("highScore", 0);

        PlayerPrefs.SetString("vehicleKey", "nuclear");
        PlayerPrefs.SetString("ownedVehicles", "nuclear/");

        PlayerPrefs.Save();
    }

    public void LoadGameData()
    {
        if (PlayerPrefs.HasKey("coins"))
            accountData.coins = PlayerPrefs.GetInt("coins");
        else
            accountData.coins = 0;

        if (PlayerPrefs.HasKey("uniqueCoins"))
            accountData.uniqueCoins = PlayerPrefs.GetInt("uniqueCoins");
        else
            accountData.uniqueCoins = 0;

        if (PlayerPrefs.HasKey("highScore"))
        {
            accountData.highScore = PlayerPrefs.GetInt("highScore");
            ScoreControl.highScore = accountData.highScore;
        }
        else
            accountData.highScore = 0;

        if (PlayerPrefs.HasKey("vehicleKey"))
            accountData.currentVehicleKey = PlayerPrefs.GetString("vehicleKey");
        else
            accountData.currentVehicleKey = "nuclear";

        if (PlayerPrefs.HasKey("ownedVehicles"))
            accountData.ownedVehicles = PlayerPrefs.GetString("ownedVehicles");
        else
            accountData.ownedVehicles = "nuclear/";
    }
}
