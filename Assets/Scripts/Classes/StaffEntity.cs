﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaffEntity : MonoBehaviour
{
    public int coinPrice;
    public int uniqueCoinPrice;

    public string staffName;

    public bool isPurchased;

    public string key;
    public int heartSize;
}
