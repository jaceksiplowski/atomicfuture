﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpawnerFollow : MonoBehaviour
{
    public GameObject followOn;
    public bool freezeX;
    public float forwardDistance = 15;

    // Update is called once per frame
    void FixedUpdate()
    {
       // forwardDistance = Math.Abs(forwardDistance);

        if (freezeX)
            this.transform.position = new Vector3(
                    transform.position.x,
                    followOn.transform.position.y + forwardDistance,
                    transform.position.z
                );

        if (!freezeX)
            this.transform.position = new Vector3(
                    followOn.transform.position.x,
                    followOn.transform.position.y + forwardDistance,
                    transform.position.z
                );
    }
}
