﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl2 : MonoBehaviour
{
    public AudioSource mainThemeBasic;
    public AudioSource mainThemeDrums;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GameAudio()
    {
        mainThemeDrums.volume = 1;
    }

    public void MenuAudio()
    {
        mainThemeDrums.volume = 0;
    }
}
