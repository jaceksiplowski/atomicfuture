﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGColorControl : MonoBehaviour
{
    Camera bgCamera;
    Color currentColor;
    Color startColor;
    int currentColorIndex = 0;
    float t = 0f;

    public Color[] colors;

    public Color color1;
    public Color color2;

    private Color colorSwap;

    // Start is called before the first frame update
    void Start()
    {
        ScoreControl.onAllStars += ChangeColor;

        bgCamera = GetComponent<Camera>();

        startColor = bgCamera.backgroundColor;
        currentColor = bgCamera.backgroundColor;

        bgCamera.backgroundColor = colors[currentColorIndex];
    }

    private void FixedUpdate()
    {

        Color nextColor = new Color();

        if (t >= 1 && currentColorIndex < colors.Length - 2)
        {
            t = 0;
            ++currentColorIndex;
            nextColor = colors[currentColorIndex + 1];
        }

        else if (currentColorIndex >= colors.Length - 1)
        {
            t = 0;
            nextColor = colors[1];
            currentColorIndex = 0;
        }
            

        t += 0.01f;
        bgCamera.backgroundColor = Color.Lerp(colors[currentColorIndex], nextColor, t);

       

        
    }

    void ChangeColor()
    {
        if (currentColorIndex < colors.Length-1)
            ++currentColorIndex;
        else
            currentColorIndex = 0;

        //bgCamera.backgroundColor = Color.Lerp(colors[currentColorIndex-1], colors[currentColorIndex], 0.1f);
    }
}
