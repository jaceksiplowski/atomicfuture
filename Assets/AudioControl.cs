﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour
{
    public AudioSource mainTheme;
    public AudioSource coinSFX;
    public AudioSource gromSFX;
    public AudioSource windSFX;
    public AudioSource bonusSFX;
    public AudioSource heartSFX;

    public AudioHighPassFilter mainThemeHPFilter;

    public AudioClip[] mainThemeClips;
    public AudioClip[] coinSFXNotesClips;
    public int coinSFXNotesClipsIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        mainThemeHPFilter.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
       // MainThemeControl();
    }

    public void PlayCoinSfx()
    {
        /*
        if(coinSFXNotesClipsIndex < coinSFXNotesClips.Length-1)
        {
            coinSFX.clip = coinSFXNotesClips[coinSFXNotesClipsIndex];
            ++coinSFXNotesClipsIndex;
        }
        */

        coinSFX.Play();
    }

    public void ResetCoinNote()
    {
        coinSFXNotesClipsIndex = 0;
        coinSFX.clip = coinSFXNotesClips[0];
    }

    public void PlayCollisionSfx()
    {
        StartCoroutine(HPMainThemeEffect(gromSFX));
        gromSFX.Play();
    }

    public void PlayWindSfx()
    {
        windSFX.Play();
    }

    public void PlayBonusSfx()
    {
        bonusSFX.Play();
    }

    public void PlayHeartSfx()
    {
        heartSFX.Play();
    }

    private IEnumerator HPMainThemeEffect(AudioSource sfxSource)
    {
        mainThemeHPFilter.enabled = true;
        yield return new WaitForSeconds(sfxSource.clip.length);
        mainThemeHPFilter.enabled = false;
    }

    public void MainThemeControl()
    {
        if(!mainTheme.isPlaying)
        {
            mainTheme.clip = mainThemeClips[Random.Range(0, mainThemeClips.Length)];
            mainTheme.Play();
        }
    }

    public void OnMenuAudio()
    {
        mainThemeHPFilter.enabled = true;
    }

    public void OnGameAudio()
    {
        mainThemeHPFilter.enabled = false;
    }
}
